HTML5 tower defense game made with Pixi.js

Running on mobile (Android):

1) Copy files from "source" directory of this repository to "assets" directory of your android project.

2) Change event listeners click - tap, mouseover - touchstart and etc. (it will work without chaning, but for example click instead of tap fires two events).

3) Create webview and load up index.html from assets folder (enable javascript for webview).

------------------------------------------

Running on web:

1) Clone this repository.

2) Open up through WebStorm or start your own server pointing at index.html, hopefully node.js...

---------------------------------------

Gameplay: 

1) No network connection needed.

2) Levels partially defined in json.

3) Build towers by simply tapping/clicking on a tile.

4) Upgrade towers by tapping/clicking on tower again.

5) No return or state saving at the moment. (game restarts on return from home screen).

6) No way to go back to menu.

7) No game over messages.