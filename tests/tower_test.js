describe("Tower attack (laser)", function () {
    "use strict";
    var stage = new PIXI.Stage;
    var tower = new Tower(null, null);
    var enemy = { position: {x: 0, y: 0}};
    tower.setStageReference(stage);

    it("sprite shouldn't be removed when it doesn't exist", function () {
        spyOn(stage, "removeChild");
        tower.stopAnimatingLaser(stage);
        expect(stage.removeChild).not.toHaveBeenCalled();
    });

    it("sprite should be removed when it exists", function () {
        tower.createAttackSprite(0, enemy);
        spyOn(stage, "removeChild");
        tower.stopAnimatingLaser(stage);
        expect(stage.removeChild).toHaveBeenCalled();
    });
});

describe("Tower enemy attack", function () {
    "use strict";
    var tower = new Tower(null, null);

    it("shouldn't attack if there's no enemy to attack", function () {
        spyOn(tower, "createAttackSprite");
        tower.attackEnemy([]);
        expect(tower.createAttackSprite).not.toHaveBeenCalled();
    });
});

describe("Tower build", function () {
    "use strict";
    var ui = {};
    ui.getCash = function () {
        return 0;
    };
    ui.setCash = function (number) {
    };
    var tower = new Tower(ui, null);
    var stage = new PIXI.Stage;
    var tile = {position: {x: 0, y: 0}, width: 0, scale: {x: 0}};

    it("should add sprite to stage", function () {
        spyOn(stage, "addChild");
        tower.build(tile, stage);
        expect(stage.addChild).toHaveBeenCalled();
    });
});

describe("Tower save", function () {
    "use strict";
    var tower = new Tower(null, null);
    var tile = {position: {x: 0, y: 0}, width: 0, scale: {x: 0}};

    it("should save tile data", function () {
        tower.saveTileValues(tile);
        expect(tower.position.x).toEqual(0);
    });
});