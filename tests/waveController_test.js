describe("Wave controller", function () {
    "use strict";
    var json = {name: "level", wave: 30, lives: 30, money: 30, size: 2};
    var stage = new PIXI.Stage;
    var infoUI = new InfoUi(json, stage);
    var tileSet = {children: [
        {position: {x: 0, y: 0},
            value: 0},
        {position: {x: 0, y: 1},
            value: 1},
        {position: {x: 1, y: 0},
            value: 2},
        {position: {x: 1, y: 1},
            value: 3}
    ], stage: stage};
    var waveController = new WaveController(json, tileSet, infoUI);
    var enemyVars = {
        container: new PIXI.SpriteBatch(),
        tiles: tileSet,
        gates: {},
        path: []
    };

});