describe("Menu Stage", function () {
    "use strict";
    var json = {"levels": [
        {name: "level", wave: 30, lives: 30, money: 30}
    ]};
    var stage = new PIXI.Stage;
    var menu = new MenuStage(stage, json);
    var gameStage = new GameStage(stage, json.levels);

    it("update should not call next update when game stage isn't defined", function () {
        spyOn(gameStage, "update");
        menu.update();
        expect(gameStage.update).not.toHaveBeenCalled();
    });
});