describe("Info UI", function () {
    "use strict";
    var json = {name: "level", wave: 30, lives: 30, money: 30};
    var stage = new PIXI.Stage;
    var infoUI = new InfoUi(json, stage);

    it("should be added to stage", function () {
        spyOn(stage, "addChild");
        infoUI.createInfoUI();
        expect(stage.addChild).toHaveBeenCalled();
    });
});