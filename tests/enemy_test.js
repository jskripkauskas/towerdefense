describe("Enemy", function () {
    "use strict";
    var json = {name: "level", wave: 30, lives: 30, money: 30, size: 2};
    var stage = new PIXI.Stage;
    var infoUI = new InfoUi(json, stage);
    infoUI.setCash = function (money) {
    };
    var tileSet = {children: [
        {position: {x: 0, y: 0},
            value: 0},
        {position: {x: 0, y: 1},
            value: 1},
        {position: {x: 1, y: 0},
            value: 2},
        {position: {x: 1, y: 1},
            value: 3}
    ], stage: stage};
    var enemyVars = {
        container: new PIXI.SpriteBatch(),
        tiles: tileSet,
        gates: {start: {x: 0, y: 0}},
        path: []
    };
    var enemy = new Enemy(infoUI, enemyVars, 800);

    it("should be marked dead/killed once it has no health", function () {
        enemy.setHealth(0);
        expect(enemy.killed).toBe(true);
    });

    it("sprite position should be equal to given position", function () {
        var sprite = enemy.createEnemy(0);
        expect(sprite.position.x).toEqual(enemyVars.gates.start.x);
    });

});